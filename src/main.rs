use egui_miniquad as egui_mq;
use miniquad as mq;

use quad_snd::{AudioCallback, AudioDeviceImpl, AudioParams, AudioSystem};
use std::sync::{Arc, RwLock};

// patchbay
mod patchbay;
use patchbay::{Patchbay, PatchbayPreset, PortData};
mod colors;
mod note;
mod tools;
use tools::load_texture;

/// music sample rate to use
const SAMPLE_RATE: usize = 44_100;

type GeneratorFunction = Box<dyn FnMut(&mut [f32], usize) + Send>;
struct Generator {
    func: GeneratorFunction,
}
impl AudioCallback for Generator {
    fn callback(&mut self, out: &mut [f32], frames: usize) {
        (self.func)(out, frames);
    }
}

/// keeps track of popup states
struct AppPopups {
    /// about & version popup
    about: bool,
}
impl AppPopups {
    /// default popup state
    pub fn default() -> AppPopups {
        AppPopups { about: false }
    }
}

/// a reference to a patchbay module
/// used to add new module to patchbay
/// as patchbay module modules don't have display names
/// the string is set to the display name
type AppPatchbayModuleDefinition = (String, Box<dyn Fn()>);

struct App {
    /// base app id
    /// used for midi connection
    id: String,
    /// app name
    /// used for title bar app name
    name: String,
    /// app icon
    /// texture id
    icon: egui::TextureId,
    /// stores state of popups
    popups: AppPopups,
    /// reference to the patchbay
    /// also handles audio generation
    patchbay: Arc<RwLock<Patchbay>>,
    /// list of batchbaymodules by category
    /// list of categories > list of modules belonging to the given category
    patchbay_modules: Vec<(String, Vec<AppPatchbayModuleDefinition>)>,
    /// presets to be used in patchbay
    presets: Vec<PatchbayPreset>,
    /// reference to egui miniquad
    egui_mq: egui_mq::EguiMq,
}
impl App {
    /// initialized basic app state
    pub fn new(ctx: &mut mq::Context) -> App {
        // setup icon
        let texture = load_texture(
            ctx,
            #[cfg(target_os = "android")]
            "icon.png",
            #[cfg(not(target_os = "android"))]
            "assets/icon.png",
        )
        .unwrap();
        let egui_tid = egui::TextureId::User(texture.gl_internal_id() as u64);

        let mut app = App {
            id: String::from("polarplayer-studio"),
            name: String::from("PolarPlayer Studio"),
            popups: AppPopups::default(),
            icon: egui_tid,
            egui_mq: egui_mq::EguiMq::new(ctx),
            patchbay_modules: Vec::new(),
            patchbay: Arc::new(RwLock::new(Patchbay::new())),
            presets: Vec::new(),
        };

        // setup patchbay
        let pb = Arc::clone(&app.patchbay);
        let spkr = pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
            patchbay::modules::sound::SpeakerModule::new(),
        ))));
        pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
            patchbay::modules::sound::GeneratorModule::new(),
        ))));

        #[cfg(target_os = "linux")]
        {
            pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                patchbay::modules::source::MidiSource::new(),
            ))));
        }

        app.patchbay_modules = vec![
            (
                "Sources".into(),
                vec![
                    #[cfg(target_os = "linux")]
                    (
                        "Midi Input".into(),
                        Box::new({
                            let pb = pb.clone();
                            move || {
                                pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                                    patchbay::modules::source::MidiSource::new(),
                                ))));
                            }
                        }),
                    ),
                    (
                        "Manual Input".into(),
                        Box::new({
                            let pb = pb.clone();
                            move || {
                                pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                                    patchbay::modules::source::ManualSource::new(),
                                ))));
                            }
                        }),
                    ),
                    (
                        "Random Input".into(),
                        Box::new({
                            let pb = pb.clone();
                            move || {
                                pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                                    patchbay::modules::source::RandomSource::new(),
                                ))));
                            }
                        }),
                    ),
                ],
            ),
            (
                "Generator".into(),
                vec![(
                    "Wave".into(),
                    Box::new({
                        let pb = pb.clone();
                        move || {
                            pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                                patchbay::modules::sound::GeneratorModule::new(),
                            ))));
                        }
                    }),
                )],
            ),
            (
                "Mods".into(),
                vec![(
                    "Note Merger".into(),
                    Box::new({
                        let pb = pb.clone();
                        move || {
                            pb.write().unwrap().add_node(Arc::new(RwLock::new(Box::new(
                                patchbay::modules::sourcemod::NoteMergeModule,
                            ))));
                        }
                    }),
                )],
            ),
        ];

        let storage = &mut quad_storage::STORAGE.lock().unwrap();
        if let Some(dt) = storage.get("presets") {
            if let Ok(json) = serde_json::from_str(&dt) {
                app.presets = json;
            }
        }

        // setup audio
        std::thread::spawn(move || {
            let mut device = AudioSystem::open_device(
                AudioParams {
                    freq: SAMPLE_RATE,
                    channels: 2,
                },
                |_spec| Generator {
                    func: Box::new(move |out, frames| {
                        for dt in 0..frames {
                            pb.write().unwrap().reset_cache();

                            if let PortData::Sound(val) = pb.read().unwrap().tick(spkr) {
                                out[dt * 2] = val;
                                out[dt * 2 + 1] = -val;
                            }
                        }
                    }),
                },
            );

            device.resume().expect("Audio playback failed");
        });

        app
    }
}

impl mq::EventHandler for App {
    fn update(&mut self, _ctx: &mut mq::Context) {}
    fn draw(&mut self, ctx: &mut mq::Context) {
        ctx.clear(Some((1., 1., 1., 1.)), None, None);
        ctx.begin_default_pass(mq::PassAction::clear_color(0.0, 0.0, 0.0, 1.0));
        ctx.end_render_pass();
        // Run the UI code:
        self.egui_mq.run(ctx, |_mq_ctx, egui_ctx| {
            // BEGIN EGUI
            egui::TopBottomPanel::top("top_panel").show(egui_ctx, |ui| {
                egui::menu::bar(ui, |ui| {
                    ui.menu_button("File", |ui| {
                        // quit application
                        #[cfg(not(target_arch = "wasm32"))]
                        if ui.button("Quit").clicked() {
                            std::process::exit(0);
                        }
                    });
                    if ui.button("About").clicked() {
                        self.popups.about = !self.popups.about;
                    };
                });
            });

            egui::SidePanel::left("Presets").show(egui_ctx, |ui| {
                ui.add(egui::Image::new(self.icon, egui::vec2(100.0, 100.0)));
                ui.label("Presets");
                ui.horizontal(|ui| {
                    if ui.button("New +").clicked() {
                        self.presets.push(
                            self.patchbay
                                .read()
                                .unwrap()
                                .export(String::from("New Preset")),
                        );
                    }
                    if ui.button("Clear").clicked() {
                        self.patchbay.write().unwrap().clear();
                        self.patchbay
                            .write()
                            .unwrap()
                            .add_node(Arc::new(RwLock::new(Box::new(
                                patchbay::modules::sound::SpeakerModule::new(),
                            ))));
                    }
                });
                ui.separator();
                ui.add_space(5.0);
                let mut save = false;
                self.presets.retain_mut(|preset| {
                    let mut keep = true;
                    ui.group(|ui| {
                        ui.vertical(|ui| {
                            if ui.text_edit_singleline(&mut preset.name).lost_focus() {
                                // save presets
                                save = true;
                            }

                            ui.horizontal(|ui| {
                                if ui.button("Load").clicked() {
                                    self.patchbay.write().unwrap().import(preset.clone());
                                }
                                if ui.button("Save").clicked() {
                                    *preset = self
                                        .patchbay
                                        .read()
                                        .unwrap()
                                        .export(preset.name.to_string());
                                    // save presets
                                    save = true;
                                }
                                if ui.button("Delete").clicked() {
                                    keep = false;
                                    // save presets
                                    save = true;
                                }
                            });
                        });
                    });
                    ui.add_space(5.);

                    keep
                });

                if save {
                    // TODO save presets
                    if let Ok(s) = serde_json::to_string(&self.presets) {
                        let storage = &mut quad_storage::STORAGE.lock().unwrap();
                        storage.set("presets", s.as_str());
                    }
                }
            });

            egui::CentralPanel::default().show(egui_ctx, |ui| {
                ui.horizontal_wrapped(|ui| {
                    for (name, items) in &self.patchbay_modules {
                        ui.vertical(|ui| {
                            ui.group(|ui| {
                                ui.label(name);
                                ui.horizontal_wrapped(|ui| {
                                    // items go here
                                    if items.is_empty() {
                                        ui.label("Nothing to be added");
                                    } else {
                                        for (nname, addcb) in items {
                                            if ui.button(nname).clicked() {
                                                addcb();
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    }
                });

                self.patchbay.read().unwrap().render(ui);
            });

            egui::Window::new("About")
                .open(&mut self.popups.about)
                .default_width(100.0)
                .show(egui_ctx, |ui| {
                    ui.columns(2, |ui| {
                        ui[0].add(egui::Image::new(self.icon, egui::vec2(200.0, 200.0)));
                        ui[0].add_space(5.0);

                        ui[0].horizontal(|ui| {
                            ui.label(self.name.to_string() + " (" + &self.id + ")");
                        });

                        ui[1].vertical(|ui| {
                            ui.add_space(20.0);
                            ui.label("Authors: ".to_owned() + env!("CARGO_PKG_AUTHORS"));
                            ui.add_space(5.0);
                            ui.label("Description: ".to_owned() + env!("CARGO_PKG_DESCRIPTION"));
                            ui.add_space(5.0);
                            ui.label("Version: ".to_owned() + env!("CARGO_PKG_VERSION"));
                            ui.add_space(5.0);
                            ui.label(format!(
                                "This package is licensed under {}",
                                env!("CARGO_PKG_LICENSE")
                            ));
                            ui.add_space(5.0);
                            ui.label("Check out the source code:");
                            ui.hyperlink(env!("CARGO_PKG_REPOSITORY"));
                        })
                    });
                });

            // END EGUI
        });
        // Draw things behind egui here
        self.egui_mq.draw(ctx);
        // Draw things in front of egui here
        ctx.commit_frame();
    }

    fn mouse_motion_event(&mut self, _ctx: &mut mq::Context, x: f32, y: f32) {
        self.egui_mq.mouse_motion_event(x, y);
    }
    fn mouse_wheel_event(&mut self, _ctx: &mut mq::Context, dx: f32, dy: f32) {
        self.egui_mq.mouse_wheel_event(dx, dy);
    }
    fn mouse_button_down_event(
        &mut self,
        ctx: &mut mq::Context,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        self.egui_mq.mouse_button_down_event(ctx, mb, x, y);
    }
    fn mouse_button_up_event(
        &mut self,
        ctx: &mut mq::Context,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        self.egui_mq.mouse_button_up_event(ctx, mb, x, y);
    }
    fn char_event(
        &mut self,
        _ctx: &mut mq::Context,
        character: char,
        _keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        self.egui_mq.char_event(character);
    }
    fn key_down_event(
        &mut self,
        ctx: &mut mq::Context,
        keycode: mq::KeyCode,
        keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        self.egui_mq.key_down_event(ctx, keycode, keymods);
    }
    fn key_up_event(&mut self, _ctx: &mut mq::Context, keycode: mq::KeyCode, keymods: mq::KeyMods) {
        self.egui_mq.key_up_event(keycode, keymods);
    }

    fn touch_event(
        &mut self,
        ctx: &mut mq::Context,
        phase: mq::TouchPhase,
        _id: u64,
        x: f32,
        y: f32,
    ) {
        match phase {
            mq::TouchPhase::Started => {
                self.mouse_button_down_event(ctx, mq::MouseButton::Left, x, y);
            }
            mq::TouchPhase::Moved => {
                self.mouse_motion_event(ctx, x, y);
            }
            mq::TouchPhase::Ended => {
                self.mouse_button_up_event(ctx, mq::MouseButton::Left, x, y);
            }
            mq::TouchPhase::Cancelled => {}
        }
    }
}

fn main() {
    let conf = mq::conf::Conf {
        window_title: "Polarplayer Studio".to_owned(),
        high_dpi: true,
        fullscreen: false,
        ..Default::default()
    };

    mq::start(conf, |ctx| Box::new(App::new(ctx)));
}
