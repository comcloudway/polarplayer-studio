use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};
pub mod modules;
use egui::Pos2;
use serde::{Deserialize, Serialize};

use self::modules::{PatchbayModule, PatchbayModulePresets};
use crate::note::Note;

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub enum PortType {
    Source,
    Sound,
}
impl PortType {
    pub fn to_color(&self) -> egui::Color32 {
        match self {
            Self::Source => egui::Color32::from_rgb(0xd7, 0x99, 0x21),
            Self::Sound => egui::Color32::from_rgb(0x83, 0xa5, 0x98),
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub enum PortData {
    Source(HashMap<u8, Note>),
    Sound(f32),
}

/// represents a patchbay node
#[derive(Clone)]
pub struct Node {
    pub module: Arc<RwLock<Box<dyn PatchbayModule>>>,
    pub inputs: Vec<usize>,
    pub output: Option<usize>,
}
/// represents a node port
#[derive(Clone, Serialize, Deserialize)]
pub struct Port {
    pub name: String,
    pub kind: PortType,
}
impl Port {
    pub fn new<S>(name: S, kind: PortType) -> Self
    where
        S: Into<String>,
    {
        Port {
            name: name.into(),
            kind,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct NodePreset {
    inputs: Vec<usize>,
    output: Option<usize>,
    module: PatchbayModulePresets,
}
#[derive(Clone, Serialize, Deserialize)]
pub struct PatchbayPreset {
    nodes: HashMap<usize, NodePreset>,
    node_positions: HashMap<usize, (f32, f32)>,
    next_node: usize,

    ports: HashMap<usize, Port>,
    next_port: usize,

    links: Vec<(usize, usize)>,
    outputs: HashMap<usize, usize>,
    inputs: HashMap<usize, usize>,

    pub name: String,
}

/// the patchbay
pub struct Patchbay {
    /// store nodes by id
    nodes: Arc<RwLock<HashMap<usize, Node>>>,
    /// next node id to be used
    next_node: usize,
    /// store ports by id
    ports: Arc<RwLock<HashMap<usize, Port>>>,
    /// next port id to be used
    next_port: usize,
    /// store links (from, to)
    links: Arc<RwLock<Vec<(usize, usize)>>>,
    /// output ports and parent nodes
    /// (port, node)
    outputs: Arc<RwLock<HashMap<usize, usize>>>,
    /// input ports and parent nodes
    /// (port, node)
    inputs: Arc<RwLock<HashMap<usize, usize>>>,
    /// cache old results
    cache: Arc<RwLock<HashMap<usize, PortData>>>,
    /// store graph context
    ctx: RwLock<egui_nodes::Context>,
}
impl Patchbay {
    /// init empty default patchbay
    pub fn new() -> Patchbay {
        let mut ctx = egui_nodes::Context::default();
        ctx.style.colors = crate::colors::gruvbox_dark_patchbay();
        ctx.style.colors[egui_nodes::ColorStyle::GridBackground as usize] =
            egui::epaint::Color32::TRANSPARENT;
        ctx.attribute_flag_push(egui_nodes::AttributeFlags::EnableLinkDetachWithDragClick);

        Patchbay {
            links: Arc::new(RwLock::new(Vec::new())),
            nodes: Arc::new(RwLock::new(HashMap::new())),
            ports: Arc::new(RwLock::new(HashMap::new())),
            outputs: Arc::new(RwLock::new(HashMap::new())),
            inputs: Arc::new(RwLock::new(HashMap::new())),
            next_node: 0,
            next_port: 0,
            cache: Arc::new(RwLock::new(HashMap::new())),
            ctx: RwLock::new(ctx),
        }
    }

    /// exports the patchbay nodes as a new preset
    pub fn export(&self, name: String) -> PatchbayPreset {
        let ctx = Arc::new(self.ctx.read().unwrap());
        PatchbayPreset {
            name,
            node_positions: self
                .nodes
                .read()
                .unwrap()
                .clone()
                .keys()
                .map(|id| {
                    let pos = ctx.get_node_pos_grid_space(*id);
                    if let Some(pos) = pos {
                        (*id, (pos.x, pos.y))
                    } else {
                        (*id, (0., 0.))
                    }
                })
                .collect(),
            nodes: self
                .nodes
                .read()
                .unwrap()
                .clone()
                .iter()
                .map(|(key, node)| {
                    let module = node.module.read().unwrap().export();
                    (
                        *key,
                        NodePreset {
                            module,
                            inputs: node.inputs.to_vec(),
                            output: node.output,
                        },
                    )
                })
                .collect(),
            next_node: self.next_node,
            ports: self.ports.read().unwrap().clone(),
            next_port: self.next_port,
            links: self.links.read().unwrap().clone(),
            outputs: self.outputs.read().unwrap().clone(),
            inputs: self.inputs.read().unwrap().clone(),
        }
    }
    /// imports a new preset
    /// overwriting the old entries
    pub fn import(&mut self, preset: PatchbayPreset) {
        *self.nodes.write().unwrap() = HashMap::new();
        for (key, node) in preset.nodes.iter() {
            if let Some(module) = node.module.construct() {
                self.nodes.write().unwrap().insert(
                    *key,
                    Node {
                        module: Arc::new(RwLock::new(module)),
                        inputs: node.inputs.to_vec(),
                        output: node.output,
                    },
                );
            }
        }
        self.next_node = preset.next_node;
        *self.ports.write().unwrap() = preset.ports;
        self.next_port = preset.next_port;
        *self.links.write().unwrap() = preset.links;
        *self.inputs.write().unwrap() = preset.inputs;
        *self.outputs.write().unwrap() = preset.outputs;

        for (id, pos) in preset.node_positions.iter() {
            self.ctx
                .write()
                .unwrap()
                .set_node_pos_grid_space(*id, Pos2::new(pos.0, pos.1))
        }
    }

    pub fn clear(&mut self) {
        *self.nodes.write().unwrap() = HashMap::new();
        self.next_node = 0;
        *self.ports.write().unwrap() = HashMap::new();
        self.next_port = 0;
        *self.links.write().unwrap() = Vec::new();
        *self.inputs.write().unwrap() = HashMap::new();
        *self.outputs.write().unwrap() = HashMap::new();
    }

    /// registers a node
    /// automatically creates input and output entries
    pub fn add_node(&mut self, module: Arc<RwLock<Box<dyn PatchbayModule>>>) -> usize {
        let index = self.next_node;
        self.next_node += 1;
        let ips = module.read().unwrap().inputs();
        let op = module.read().unwrap().output();
        let node = Node {
            inputs: ips.iter().map(|p| self.add_port(p.clone())).collect(),
            output: op.map(|p| self.add_port(p)),
            module,
        };
        node.module.write().unwrap().set_id(index);
        for ip in &node.inputs {
            self.inputs.write().unwrap().insert(*ip, index);
        }
        if let Some(op) = &node.output {
            self.outputs.write().unwrap().insert(*op, index);
        }
        self.nodes.write().unwrap().insert(index, node);
        index
    }

    /// registers a new port
    pub fn add_port(&mut self, port: Port) -> usize {
        let index = self.next_port;
        self.next_port += 1;

        self.ports.write().unwrap().insert(index, port);

        index
    }

    pub fn get_node_outputs(&self, node_id: usize) -> Option<usize> {
        if self.nodes.read().unwrap().get(&node_id).is_some() {
            for (pid, nid) in &*self.outputs.read().unwrap() {
                if node_id == *nid {
                    return Some(*pid);
                }
            }

            None
        } else {
            None
        }
    }

    pub fn get_node_inputs(&self, node_id: usize) -> Option<Vec<usize>> {
        if self.nodes.read().unwrap().get(&node_id).is_some() {
            let mut v: Vec<usize> = Vec::new();

            for (pid, nid) in &*self.inputs.read().unwrap() {
                if node_id == *nid {
                    v.push(*pid);
                }
            }

            Some(v)
        } else {
            None
        }
    }

    pub fn render(&self, ui: &mut egui::Ui) {
        let mut nodes = Vec::new();

        let ni: Vec<usize> = self.nodes.read().unwrap().keys().copied().collect();
        for id in ni {
            let ns = Arc::clone(&self.nodes);
            let mut n = egui_nodes::NodeConstructor::new(
                id,
                egui_nodes::NodeArgs {
                    background: Some(egui::Color32::from_gray(40)),
                    background_hovered: Some(egui::Color32::from_gray(40)),
                    background_selected: Some(egui::Color32::from_gray(40)),
                    titlebar: Some(egui::Color32::TRANSPARENT),
                    titlebar_selected: Some(egui::Color32::TRANSPARENT),
                    titlebar_hovered: Some(egui::Color32::TRANSPARENT),
                    corner_rounding: Some(4.0),
                    ..egui_nodes::NodeArgs::default()
                },
            );

            let is = Arc::clone(&self.inputs);
            let os = Arc::clone(&self.outputs);
            let ps = Arc::clone(&self.ports);
            let inps = self.get_node_inputs(id).unwrap_or_default().clone();
            let outs = self.get_node_outputs(id);
            let inp = inps.clone();
            let title = self
                .nodes
                .read()
                .unwrap()
                .get(&id)
                .unwrap()
                .module
                .read()
                .unwrap()
                .to_str()
                .to_string();
            n = n.with_title(move |ui| {
                let res = ui.label(title);
                {
                    ns.write()
                        .unwrap()
                        .get_mut(&id)
                        .unwrap()
                        .module
                        .write()
                        .unwrap()
                        .render(ui);
                }
                if id != 0 && ui.button("Delete 🗑").clicked() {
                    // remove port table
                    if let Some(p) = outs {
                        os.write().unwrap().remove(&p);
                        ps.write().unwrap().remove(&p);
                    }
                    for p in inp {
                        is.write().unwrap().remove(&p);
                        ps.write().unwrap().remove(&p);
                    }
                    // finally remove id
                    ns.write().unwrap().remove(&id);
                }
                res
            });

            for port in inps {
                let name = self
                    .ports
                    .read()
                    .unwrap()
                    .get(&port)
                    .unwrap()
                    .name
                    .to_string();
                let kind = self
                    .ports
                    .read()
                    .unwrap()
                    .get(&port)
                    .unwrap()
                    .kind
                    .to_color();
                let mut args = egui_nodes::PinArgs::new();
                args.background = Some(kind);
                args.hovered = Some(kind.linear_multiply(0.8));
                n = n.with_input_attribute(port, args, move |ui| ui.label(name));
            }
            if let Some(port) = outs {
                let name = self
                    .ports
                    .read()
                    .unwrap()
                    .get(&port)
                    .unwrap()
                    .name
                    .to_string();
                let kind = self
                    .ports
                    .read()
                    .unwrap()
                    .get(&port)
                    .unwrap()
                    .kind
                    .to_color();
                let mut args = egui_nodes::PinArgs::new();
                args.background = Some(kind);
                args.hovered = Some(kind.linear_multiply(0.8));
                n = n.with_output_attribute(port, args, move |ui| ui.label(name));
            }

            nodes.push(n);
        }
        // add them to the ui
        let mut links = self.links.write().unwrap();
        *links = links
            .iter()
            .filter(|(s, e)| {
                let l = self.ports.read().unwrap();
                let a = l.contains_key(e);
                let b = l.contains_key(s);
                a && b
            })
            .map(|(a, b)| (*a, *b))
            .collect();

        let ports = self.ports.clone();
        self.ctx.write().unwrap().show(
            nodes,
            links.iter().enumerate().map(|(i, (start, end))| {
                let mut args = egui_nodes::LinkArgs::default();
                if let Some(port) = ports.read().unwrap().get(start) {
                    let base = port.kind.to_color();
                    args.base = Some(base);
                    args.hovered = Some(base.linear_multiply(0.5));
                    args.selected = Some(base.linear_multiply(0.8));
                }
                (i, *start, *end, args)
            }),
            ui,
        );

        // remove destroyed links
        if let Some(idx) = self.ctx.read().unwrap().link_destroyed() {
            if idx < links.len() {
                links.remove(idx);
            }
        }

        // add created links
        if let Some((start, end, _)) = self.ctx.read().unwrap().link_created() {
            if self.ports.read().unwrap().get(&start).unwrap().kind
                == self.ports.read().unwrap().get(&end).unwrap().kind
            {
                // ports have to have same type in order to be connected
                links.push((start, end))
            }
        }
    }

    /// resets the cache, for new buffer iteration
    pub fn reset_cache(&self) {
        self.cache.write().unwrap().clear();
    }

    pub fn tick(&self, id: usize) -> PortData {
        let cl = self.cache.read().unwrap();
        if let Some(dt) = cl.get(&id) {
            let dt = dt.clone();
            drop(cl);
            dt
        } else {
            drop(cl);
            let inp = self.get_node_inputs(id).unwrap();
            let out = self.outputs.read().unwrap();
            let links: Vec<(usize, usize)> = self
                .links
                .read()
                .unwrap()
                .clone()
                .iter()
                .filter(|(b, e)| inp.contains(e) && out.get(b).is_some())
                .map(|(b, e)| (*e, *out.get(b).unwrap()))
                .collect();
            drop(out);

            if links.len() < inp.len() {
                return PortData::Sound(0.0);
            }

            // handle data resolving
            if let Some(dt) = (*self.nodes.read().unwrap()).get(&{ id }) {
                let arc = Arc::clone(&dt.module);
                let res = arc.read().unwrap().tick(self, links);

                let mut cl = self.cache.write().unwrap();
                cl.insert(id, res.clone());
                drop(cl);

                res
            } else {
                PortData::Sound(0.0)
            }
        }
    }
}
