use egui::{self, plot::PlotPoints};
use serde::{Deserialize, Serialize};

use super::super::{Patchbay, PatchbayModule};
use crate::{
    patchbay::{modules::PatchbayModulePreset, Port, PortData, PortType},
    tools::midinote_to_hz,
    SAMPLE_RATE,
};
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
/// a collection of all wave types
pub enum Wave {
    Sine,
    Square,
    Sawtooth,
    Triangle,
}
impl Wave {
    /// initialised the default Wave generator
    pub fn default() -> Wave {
        Wave::Sine
    }
    /// returns the name of the current wave
    pub fn to_str(&self) -> String {
        match self {
            Wave::Sine => String::from("Sine"),
            Wave::Square => String::from("Square"),
            Wave::Sawtooth => String::from("Sawtooth"),
            Wave::Triangle => String::from("Triangle"),
        }
    }
    /// gets value at given point
    pub fn yfx(&self, x: f32) -> f32 {
        match self {
            Wave::Sine => f32::sin(x * std::f32::consts::PI * 2.0),
            Wave::Sawtooth => {
                let p = 1.0;
                ((x % p) + p) % p
            }
            Wave::Triangle => {
                let a = 1.0;
                let p = 1.0;
                4.0 * a / p * f32::abs((((x - p / 4.0) % p) + p) % p - p / 2.0) - a
            }
            Wave::Square => {
                let base = f32::sin(x * std::f32::consts::PI * 2.0);
                if base < 0.0 {
                    -1.0
                } else if base == 0.0 {
                    0.0
                } else {
                    1.0
                }
            }
        }
    }
}

/// Control data for the chord mode
/// used to determine frequencies for a note
/// should return all frequencies in Hz for a given chord
/// used to press one key and play the corresponding chord
#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub enum Chord {
    /// chord mode disabled
    /// should only return the given notes frequency to herz
    None,
    /// play a major chord
    /// 4;3
    Major,
    /// play a minor chord
    /// 3;4
    Minor,
}
impl Chord {
    /// default chord
    pub fn default() -> Chord {
        Chord::None
    }

    /// generate chord
    pub fn midi_to_freq(&self, note: u8) -> Vec<f32> {
        match self {
            Chord::None => vec![midinote_to_hz(note)],
            Chord::Major => vec![
                midinote_to_hz(note),
                midinote_to_hz(note + 4),
                midinote_to_hz(note + 3 + 4),
            ],
            Chord::Minor => vec![
                midinote_to_hz(note),
                midinote_to_hz(note + 3),
                midinote_to_hz(note + 4 + 3),
            ],
        }
    }

    /// get chord name
    pub fn name(&self) -> &str {
        match self {
            Chord::None => "No",
            Chord::Major => "Major",
            Chord::Minor => "Minor",
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct GeneratorModulePreset {
    wave: Wave,
    chord: Chord,
    id: Option<usize>,
    pitch_offset: i8,
    pitch_enabled: bool,
}
impl PatchbayModulePreset for GeneratorModulePreset {
    fn construct(&self) -> Box<dyn PatchbayModule> {
        let mut src = GeneratorModule::new();

        src.wave = RwLock::new(self.wave.clone());
        src.chord = RwLock::new(self.chord.clone());
        src.pitch_offset = RwLock::new(self.pitch_offset);
        src.pitch_enabled = Arc::new(RwLock::new(self.pitch_enabled));
        if let Some(id) = self.id {
            src.set_id(id);
        }

        Box::new(src)
    }
}

/// the generator module
/// used to generate waves for the active notes
pub struct GeneratorModule {
    pub wave: RwLock<Wave>,
    pub chord: RwLock<Chord>,
    pub pitch_offset: RwLock<i8>,
    id: Option<usize>,
    phase: Arc<RwLock<HashMap<u8, f32>>>,
    pub pitch_enabled: Arc<RwLock<bool>>,
}
impl GeneratorModule {
    pub fn new() -> GeneratorModule {
        GeneratorModule {
            wave: RwLock::new(Wave::default()),
            chord: RwLock::new(Chord::default()),
            pitch_offset: RwLock::new(0),
            id: None,
            phase: Arc::new(RwLock::new(HashMap::new())),
            pitch_enabled: Arc::new(RwLock::new(true)),
        }
    }
}
impl PatchbayModule for GeneratorModule {
    fn to_str(&self) -> String {
        String::from("Generator")
    }
    fn output(&self) -> Option<Port> {
        Some(Port::new("Sound output", PortType::Sound))
    }
    fn inputs(&self) -> Vec<Port> {
        vec![Port::new("Note input", PortType::Source)]
    }
    fn set_id(&mut self, id: usize) {
        self.id = Some(id);
    }
    fn tick(&self, pb: &Patchbay, inp: Vec<(usize, usize)>) -> PortData {
        let mut current = 0.0;

        if let Some(inp) = inp.first() {
            if let PortData::Source(notes) = pb.tick(inp.1) {
                for note in notes.values() {
                    if !self.phase.read().unwrap().contains_key(&note.freq) {
                        self.phase.write().unwrap().insert(note.freq, 0.0);
                    }
                    // calculate pitch
                    let pc = ((note.pitch.1 as u32) << 7) | note.pitch.0 as u32;
                    let mut pitch = 1.0;
                    if pc != 0 && *self.pitch_enabled.read().unwrap() {
                        pitch = pc as f32 / 8192.0;
                    }
                    // play chord
                    let mut f = note.freq;
                    if f >= self.pitch_offset.read().unwrap().unsigned_abs() {
                        let n = f as i16 + *self.pitch_offset.read().unwrap() as i16;
                        if n <= 127 {
                            f = n as u8;
                        }
                    }
                    for freq in self.chord.read().unwrap().midi_to_freq(f).iter() {
                        current += self.wave.read().unwrap().yfx(
                            2.0 * self.phase.read().unwrap().get(&note.freq).unwrap()
                                * freq
                                * pitch,
                        ) * note.pressure as f32
                            / 127.0;
                    }
                    *self.phase.write().unwrap().get_mut(&note.freq).unwrap() +=
                        1.0 / SAMPLE_RATE as f32;
                }
            }
        }

        PortData::Sound(current)
    }
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.group(|ui| {
            ui.horizontal(|ui| {
                ui.add_space(10.0);
                ui.vertical(|ui| {
                    for wave in [Wave::Sine, Wave::Sawtooth, Wave::Square, Wave::Triangle] {
                        if ui
                            .add(egui::SelectableLabel::new(
                                *self.wave.read().unwrap() == wave,
                                wave.to_str(),
                            ))
                            .clicked()
                        {
                            *self.wave.write().unwrap() = wave;
                        }
                    }
                });
                egui::plot::Plot::new("Wave Generator Preview")
                    .show_axes([false, false])
                    .show_x(false)
                    .show_y(false)
                    .show_background(false)
                    .height(50.0)
                    .width(100.0)
                    .allow_drag(false)
                    .allow_zoom(false)
                    .center_x_axis(true)
                    .show(ui, |plot_ui| {
                        let points: PlotPoints = (0..=100)
                            .map(|i| {
                                let x = i as f32 * 0.01;
                                [x as f64, self.wave.read().unwrap().yfx(x) as f64]
                            })
                            .collect();

                        let line = egui::plot::Line::new(points);
                        plot_ui.line(line);
                    });
                ui.add_space(10.0);
            });
        });
        ui.group(|ui| {
            ui.label("Chord");
            ui.horizontal(|ui| {
                ui.selectable_value(
                    &mut *self.chord.write().unwrap(),
                    Chord::None,
                    Chord::None.name(),
                );
                ui.selectable_value(
                    &mut *self.chord.write().unwrap(),
                    Chord::Major,
                    Chord::Major.name(),
                );
                ui.selectable_value(
                    &mut *self.chord.write().unwrap(),
                    Chord::Minor,
                    Chord::Minor.name(),
                );
            });
        });
        ui.group(|ui| {
            ui.label("Pitch offset");
            ui.add(
                egui::Slider::new(&mut *self.pitch_offset.write().unwrap(), -7..=7)
                    .show_value(false),
            );
            ui.add(egui::DragValue::new(&mut *self.pitch_offset.write().unwrap()).speed(1));
            if ui.button("Reset").clicked() {
                *self.pitch_offset.write().unwrap() = 0;
            }
        });
        ui.group(|ui| {
            ui.checkbox(
                &mut self.pitch_enabled.write().unwrap(),
                "Enable pitch modification",
            );
        });
    }

    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        crate::patchbay::modules::PatchbayModulePresets::GeneratorModule(GeneratorModulePreset {
            pitch_enabled: *self.pitch_enabled.read().unwrap(),
            id: self.id,
            wave: self.wave.read().unwrap().clone(),
            chord: self.chord.read().unwrap().clone(),
            pitch_offset: *self.pitch_offset.read().unwrap(),
        })
    }
}
