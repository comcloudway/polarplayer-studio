use crate::patchbay::{modules::PatchbayModulePreset, Port, PortData, PortType};
use egui;
use serde::{Deserialize, Serialize};

use super::super::{Patchbay, PatchbayModule};

#[derive(Clone, Serialize, Deserialize)]
pub struct SpeakerModulePreset;
impl PatchbayModulePreset for SpeakerModulePreset {
    fn construct(&self) -> Box<dyn PatchbayModule> {
        Box::new(SpeakerModule::new())
    }
}

/// speaker sink module
/// used to connect other nodes and play audio
/// cannot be deleted or added
/// one patchbay can only can contain one speaker,
/// this speaker needs to have id 0
pub struct SpeakerModule;
impl SpeakerModule {
    pub fn new() -> SpeakerModule {
        SpeakerModule
    }
}
impl PatchbayModule for SpeakerModule {
    fn to_str(&self) -> String {
        String::from("Speaker")
    }
    fn set_id(&mut self, _id: usize) {}
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.label("🔈");
    }
    fn tick(&self, pb: &Patchbay, inp: Vec<(usize, usize)>) -> PortData {
        let mut data: f32 = 0.0;
        let inp: Vec<usize> = inp.iter().map(|(_p, b)| *b).collect();
        for port in inp.iter() {
            if let PortData::Sound(snd) = pb.tick(*port) {
                data += snd;
            }
        }
        PortData::Sound(data)
    }
    fn output(&self) -> Option<Port> {
        None
    }
    fn inputs(&self) -> Vec<Port> {
        vec![Port::new("Sound input", PortType::Sound)]
    }

    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        crate::patchbay::modules::PatchbayModulePresets::SpeakerModule(SpeakerModulePreset)
    }
}
