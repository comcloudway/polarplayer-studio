use super::{Patchbay, Port, PortData, PortType};
use egui;
use serde::{Deserialize, Serialize};

pub mod sound;
pub mod source;
pub mod sourcemod;

/// trait every patchbay module has to implement
pub trait PatchbayModule: Send + Sync {
    /// get module name
    fn to_str(&self) -> String;

    /// render ui components for module
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.label(self.to_str());
    }
    /// generate sound inside of module
    fn tick(&self, _pb: &Patchbay, _inp: Vec<(usize, usize)>) -> PortData {
        PortData::Sound(0.)
    }

    /// list outputs
    fn output(&self) -> Option<Port> {
        Some(Port::new("Default Output", PortType::Sound))
    }
    /// list inputs
    fn inputs(&self) -> Vec<Port> {
        Vec::new()
    }

    fn set_id(&mut self, _id: usize) {}

    fn export(&self) -> PatchbayModulePresets;
}

pub trait PatchbayModulePreset {
    fn construct(&self) -> Box<dyn PatchbayModule>;
}

#[derive(Clone, Deserialize, Serialize)]
pub enum PatchbayModulePresets {
    #[cfg(target_os = "linux")]
    MidiSource(source::MidiSourcePreset),
    ManualSource(source::ManualSource),
    RandomSource(source::RandomSourcePreset),

    NoteMergeModule(sourcemod::NoteMergeModule),

    GeneratorModule(sound::GeneratorModulePreset),
    SpeakerModule(sound::SpeakerModulePreset),
}
impl PatchbayModulePresets {
    pub fn construct(&self) -> Option<Box<dyn PatchbayModule>> {
        match self {
            #[cfg(target_os = "linux")]
            Self::MidiSource(inner) => Some(inner.construct()),
            Self::ManualSource(inner) => Some(Box::new(inner.clone())),
            Self::RandomSource(inner) => Some(inner.construct()),

            Self::GeneratorModule(inner) => Some(inner.construct()),
            Self::SpeakerModule(inner) => Some(inner.construct()),

            Self::NoteMergeModule(inner) => Some(Box::new(inner.clone())),
        }
    }
}
