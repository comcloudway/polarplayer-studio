use egui::mutex::Mutex;
use serde::{Deserialize, Serialize};

use super::super::{Patchbay, PatchbayModule, Port, PortData, PortType};
use crate::{note::Note, patchbay::modules::PatchbayModulePreset};
use std::{
    collections::HashMap,
    sync::{
        mpsc::{channel, Sender},
        Arc, RwLock,
    },
};

#[derive(Clone, Serialize, Deserialize)]
pub struct MidiSourcePreset {
    id: Option<usize>,
}
impl PatchbayModulePreset for MidiSourcePreset {
    fn construct(&self) -> Box<dyn PatchbayModule> {
        let mut src = MidiSource::new();
        if let Some(id) = self.id {
            src.set_id(id);
        }
        Box::new(src)
    }
}

enum BridgeMessage {
    /// attempt to select new port
    TrySelect(usize),
    /// rescans for ports
    Rescan,
}

pub struct MidiSource {
    id: Option<usize>,
    notes: Arc<RwLock<HashMap<u8, Note>>>,
    ports: Arc<RwLock<Vec<String>>>,
    selected: Arc<RwLock<Option<usize>>>,
    sender: Mutex<Sender<BridgeMessage>>,
}
impl MidiSource {
    pub fn new() -> Self {
        let ports = Arc::new(RwLock::new(Vec::new()));
        let sel = Arc::new(RwLock::new(None));
        let notes = Arc::new(RwLock::new(HashMap::new()));

        let (send, recv) = channel();

        std::thread::spawn({
            let sel = sel.clone();
            let notes = notes.clone();
            let ports = ports.clone();
            move || {
                let mut mi = midir::MidiInput::new("polarplayer-studio").unwrap();
                mi.ignore(midir::Ignore::None);
                let mut midi_ports = Vec::new();
                for port in mi.ports() {
                    ports.write().unwrap().push(
                        mi.port_name(&port)
                            .unwrap_or_else(|_| String::from("Unknown")),
                    );
                    midi_ports.push(port);
                }
                drop(mi);

                // used to keep connection alive
                let mut midi_listeners = HashMap::new();

                while let Ok(dt) = recv.recv() {
                    match dt {
                        BridgeMessage::Rescan => {
                            let mut mi = midir::MidiInput::new("polarplayer-studio").unwrap();
                            mi.ignore(midir::Ignore::None);
                            midi_ports.clear();
                            ports.write().unwrap().clear();
                            for port in mi.ports() {
                                ports.write().unwrap().push(
                                    mi.port_name(&port)
                                        .unwrap_or_else(|_| String::from("Unknown")),
                                );
                                midi_ports.push(port);
                            }
                        }
                        BridgeMessage::TrySelect(port) => {
                            // port does not exist
                            if midi_ports.len() <= port {
                                continue;
                            }

                            // remove old midi connection
                            if midi_listeners.get(&0).is_some() {
                                midi_listeners.remove(&0);
                            }
                            // update value
                            *sel.write().unwrap() = Some(port);

                            let mut mi = midir::MidiInput::new("polarplayer-studio").unwrap();
                            mi.ignore(midir::Ignore::None);

                            let notes = notes.clone();

                            midi_listeners.insert(
                                0,
                                mi.connect(
                                    &midi_ports[port],
                                    &format!(
                                        "polarplayer-studio-connection-{}",
                                        port.to_string().as_str()
                                    ),
                                    move |_stamp, message, _| {
                                        // communicate with midi thread
                                        let message: Vec<u8> = message.to_vec();
                                        let code = message.first().unwrap();
                                        let channel = code % 16 + 1;
                                        match code {
                                            128..=143 => {
                                                // NOTE OFF
                                                let _note = message.get(1).unwrap();
                                                let _vel = message.get(2).unwrap();
                                                notes.write().unwrap().remove(&channel);
                                            }
                                            144..=159 => {
                                                // NOTE ON
                                                let note = message.get(1).unwrap();
                                                let vel = message.get(2).unwrap();
                                                notes
                                                    .write()
                                                    .unwrap()
                                                    .insert(channel, Note::from_midi(*note, *vel));
                                            }
                                            160..=175 => {
                                                // POLYPHONIC AFTERTOUCH
                                                // TODO handle aftertouch
                                            }
                                            176..=191 => {
                                                // CONTROL/MODE CHANGE
                                            }
                                            192..=207 => {
                                                // PROGRAM CHANGE
                                            }
                                            208..=223 => {
                                                // CHANNEL AFTERTOUCH - pressure
                                                let p = message.get(1).unwrap();
                                                if let Some(note) =
                                                    notes.write().unwrap().get_mut(&channel)
                                                {
                                                    note.pressure = *p;
                                                }
                                            }
                                            224..=239 => {
                                                // PITCH WHEEL CONTROL
                                                //TODO handle pitch
                                                let p1 = message.get(1).unwrap();
                                                let p2 = message.get(2).unwrap();
                                                if let Some(note) =
                                                    notes.write().unwrap().get_mut(&channel)
                                                {
                                                    note.pitch = (*p1, *p2);
                                                }
                                            }
                                            _ => (),
                                        }
                                    },
                                    (),
                                )
                                .unwrap(),
                            );
                        }
                    }
                }
            }
        });

        Self {
            id: None,
            ports,
            notes,
            selected: sel,
            sender: Mutex::new(send),
        }
    }
}
impl PatchbayModule for MidiSource {
    /// get module name
    fn to_str(&self) -> String {
        String::from("Midi Input")
    }

    /// render ui components for module
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.group(|ui| {
            ui.vertical(|ui| {
                let list = self.ports.read().unwrap().clone();
                for (p, port) in list.iter().enumerate() {
                    if ui
                        .add(egui::SelectableLabel::new(
                            p == self
                                .selected
                                .read()
                                .unwrap()
                                .to_owned()
                                .unwrap_or(list.len()),
                            port,
                        ))
                        .clicked()
                    {
                        let _ = self.sender.lock().send(BridgeMessage::TrySelect(p));
                    }
                }
                if list.is_empty() {
                    ui.label("No Midi devices found");
                }
            })
        });

        if ui.button("Refresh").clicked() {
            let _ = self.sender.lock().send(BridgeMessage::Rescan);
        }
    }
    /// generate sound inside of module
    fn tick(&self, _pb: &Patchbay, _inp: Vec<(usize, usize)>) -> PortData {
        PortData::Source(self.notes.read().unwrap().clone())
    }

    /// list outputs
    fn output(&self) -> Option<Port> {
        Some(Port::new("Midi Notes", PortType::Source))
    }
    /// list inputs
    fn inputs(&self) -> Vec<Port> {
        Vec::new()
    }

    fn set_id(&mut self, id: usize) {
        self.id = Some(id)
    }

    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        crate::patchbay::modules::PatchbayModulePresets::MidiSource(MidiSourcePreset {
            id: self.id,
        })
    }
}
