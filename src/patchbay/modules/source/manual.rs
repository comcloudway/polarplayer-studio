use serde::{Deserialize, Serialize};

use crate::{
    note::Note,
    patchbay::{modules::PatchbayModule, Port},
};
use std::collections::HashMap;

#[derive(Clone, Serialize, Deserialize)]
pub struct ManualSource {
    /// midi note
    /// between 0 and 127
    pub freq: u8,
    /// initial note velocity
    /// between 0 and 127
    pub vel: u8,
    /// botch pitch control values
    /// will be bit shiftet to represent one larger number
    pub pitch: (u8, u8),
    /// pressure send by midi device
    pub pressure: u8,
    /// channel to output note on
    pub channel: u8,
    /// enable/disable note
    pub active: bool,
}
impl PatchbayModule for ManualSource {
    fn tick(
        &self,
        _pb: &crate::patchbay::Patchbay,
        _inp: Vec<(usize, usize)>,
    ) -> crate::patchbay::PortData {
        let mut map = HashMap::new();
        if self.active {
            map.insert(
                self.channel,
                Note {
                    freq: self.freq,
                    vel: self.vel,
                    pitch: self.pitch,
                    pressure: self.pressure,
                },
            );
        }
        crate::patchbay::PortData::Source(map)
    }
    fn to_str(&self) -> String {
        String::from("Manual Note")
    }
    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        crate::patchbay::modules::PatchbayModulePresets::ManualSource(self.clone())
    }
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.checkbox(&mut self.active, "Enable");
        ui.horizontal(|ui| {
            ui.label("Output channel");
            ui.add(egui::DragValue::new(&mut self.channel));
        });
        ui.group(|ui| {
            ui.label("Midi Note");
            ui.add(egui::Slider::new(&mut self.freq, 0..=127));
            ui.label("Initial Velocity");
            ui.add(egui::Slider::new(&mut self.vel, 0..=127));
        });
        ui.group(|ui| {
            ui.label("Pressure");
            ui.add(egui::Slider::new(&mut self.pressure, 0..=127));
        });
        ui.group(|ui| {
            ui.label("Pitch");
            ui.add(egui::Slider::new(&mut self.pitch.0, 0..=127));
            ui.add(egui::Slider::new(&mut self.pitch.1, 0..=127));
        });
    }
    fn output(&self) -> Option<crate::patchbay::Port> {
        Some(Port::new("Notes", crate::patchbay::PortType::Source))
    }
}
impl ManualSource {
    pub fn new() -> Self {
        Self {
            freq: 56,
            vel: 0,
            pitch: (0, 0),
            pressure: 0,
            active: false,
            channel: 0,
        }
    }
}
