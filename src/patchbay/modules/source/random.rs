use egui::mutex::RwLock;
use quad_rand as qrand;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    time::{SystemTime, UNIX_EPOCH},
};

use crate::{
    note::Note,
    patchbay::{
        modules::{PatchbayModule, PatchbayModulePreset, PatchbayModulePresets},
        Port,
    },
    SAMPLE_RATE,
};

#[derive(Clone, Deserialize, Serialize)]
pub struct RandomSourcePreset {
    /// value range
    range: (u8, u8),
    /// output channel
    channel: u8,
    pressure: u8,
    duration: f32,
}

pub struct RandomSource {
    /// value range
    range: (u8, u8),
    /// output channel
    channel: u8,
    pressure: u8,
    duration: f32,

    current_note: RwLock<u8>,
    phase: RwLock<f32>,
}
impl PatchbayModule for RandomSource {
    fn to_str(&self) -> String {
        String::from("Random Note")
    }
    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        PatchbayModulePresets::RandomSource(RandomSourcePreset {
            range: self.range,
            channel: self.channel,
            pressure: self.pressure,
            duration: self.duration,
        })
    }
    fn render(&mut self, ui: &mut egui::Ui) {
        ui.horizontal(|ui| {
            ui.label("Output channel");
            ui.add(egui::DragValue::new(&mut self.channel));
        });
        ui.horizontal(|ui| {
            ui.label("Pressure");
            ui.add(egui::Slider::new(&mut self.pressure, 0..=127));
        });
        ui.group(|ui| {
            ui.label("Range");
            ui.add(egui::Slider::new(&mut self.range.0, 0..=127));
            ui.add(egui::Slider::new(&mut self.range.1, 0..=127));
        });
        ui.group(|ui| {
            ui.label("Duration (in s)");
            ui.add(egui::DragValue::new(&mut self.duration));
        });
    }
    fn tick(
        &self,
        _pb: &crate::patchbay::Patchbay,
        _inp: Vec<(usize, usize)>,
    ) -> crate::patchbay::PortData {
        if self.phase.read().to_owned() >= SAMPLE_RATE as f32 * self.duration {
            // generate random note
            let note = qrand::gen_range(self.range.0, self.range.1);
            *self.current_note.write() = note;

            *self.phase.write() = 0.0;
        } else {
            *self.phase.write() += 1.0;
        }

        let mut map = HashMap::new();
        map.insert(
            self.channel,
            Note {
                freq: self.current_note.read().to_owned(),
                vel: 127,
                pitch: (0, 0),
                pressure: self.pressure,
            },
        );
        crate::patchbay::PortData::Source(map)
    }
    fn output(&self) -> Option<crate::patchbay::Port> {
        Some(Port::new("Random note", crate::patchbay::PortType::Source))
    }
}
impl RandomSource {
    pub fn new() -> Self {
        Self {
            range: (0, 127),
            duration: 1.0,
            pressure: 50,
            channel: 0,
            current_note: RwLock::new(0),
            phase: RwLock::new(0.0),
        }
    }
}
impl PatchbayModulePreset for RandomSourcePreset {
    fn construct(&self) -> Box<dyn PatchbayModule> {
        if let Ok(n) = SystemTime::now().duration_since(UNIX_EPOCH) {
            qrand::srand(n.as_secs());
        }
        Box::new(RandomSource {
            phase: RwLock::new(0.0),
            current_note: RwLock::new(0),
            range: self.range,
            channel: self.channel,
            pressure: self.pressure,
            duration: self.duration,
        })
    }
}
