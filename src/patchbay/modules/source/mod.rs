#[cfg(target_os = "linux")]
mod midi_in;
#[cfg(target_os = "linux")]
pub use midi_in::{MidiSource, MidiSourcePreset};

mod manual;
pub use manual::ManualSource;

mod random;
pub use random::{RandomSource, RandomSourcePreset};
