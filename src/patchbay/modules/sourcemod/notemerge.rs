use crate::patchbay::{modules::PatchbayModule, Port, PortData, PortType};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize, Clone)]
pub struct NoteMergeModule;
impl PatchbayModule for NoteMergeModule {
    fn to_str(&self) -> String {
        String::from("Note merger")
    }
    fn output(&self) -> Option<crate::patchbay::Port> {
        Some(Port::new("Merge out", PortType::Source))
    }
    fn inputs(&self) -> Vec<Port> {
        vec![Port::new("Multi note in", PortType::Source)]
    }
    fn export(&self) -> crate::patchbay::modules::PatchbayModulePresets {
        crate::patchbay::modules::PatchbayModulePresets::NoteMergeModule(self.clone())
    }
    fn render(&mut self, _ui: &mut egui::Ui) {}
    fn tick(&self, pb: &crate::patchbay::Patchbay, inp: Vec<(usize, usize)>) -> PortData {
        let mut map = HashMap::new();

        for (i, item) in inp.iter().enumerate() {
            if let PortData::Source(dt) = pb.tick(item.1) {
                for (key, value) in dt.iter() {
                    map.insert(key + 16 * (i as u8), value.clone());
                }
            }
        }

        PortData::Source(map)
    }
}
