use egui_nodes::ColorStyle;

pub fn gruvbox_dark_patchbay() -> [egui::Color32; ColorStyle::Count as usize] {
    let mut colors = [egui::Color32::BLACK; ColorStyle::Count as usize];
    colors[ColorStyle::NodeBackground as usize] =
        egui::Color32::from_rgba_unmultiplied(0x3c, 0x38, 0x36, 255);
    colors[ColorStyle::NodeBackgroundHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(0x50, 0x49, 0x45, 255);
    colors[ColorStyle::NodeBackgroundSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(0x50, 0x49, 0x45, 255);
    colors[ColorStyle::NodeOutline as usize] =
        egui::Color32::from_rgba_unmultiplied(0xeb, 0xdb, 0xb2, 255);
    colors[ColorStyle::TitleBar as usize] = egui::Color32::from_rgba_unmultiplied(41, 74, 122, 255);
    colors[ColorStyle::TitleBarHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::TitleBarSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::Link as usize] =
        egui::Color32::from_rgba_unmultiplied(0xdb, 0xdb, 0xb2, 200);
    colors[ColorStyle::LinkHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(0xdb, 0xdb, 0xb2, 255);
    colors[ColorStyle::LinkSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(0xd5, 0xc4, 0xa1, 255);
    colors[ColorStyle::Pin as usize] = egui::Color32::from_rgba_unmultiplied(0x98, 0x97, 0x1a, 180);
    colors[ColorStyle::PinHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(0xb8, 0xbb, 0x26, 255);
    colors[ColorStyle::BoxSelector as usize] =
        egui::Color32::from_rgba_unmultiplied(0xd3, 0x86, 0x9b, 30);
    colors[ColorStyle::BoxSelectorOutline as usize] =
        egui::Color32::from_rgba_unmultiplied(0xb1, 0x62, 0x86, 150);
    colors[ColorStyle::GridBackground as usize] =
        egui::Color32::from_rgba_unmultiplied(0x28, 0x28, 0x28, 200);
    colors[ColorStyle::GridLine as usize] =
        egui::Color32::from_rgba_unmultiplied(0xbd, 0xae, 0x93, 40);
    colors
}
