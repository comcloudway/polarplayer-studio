use serde::{Deserialize, Serialize};

/// contains all the data associated with a note
/// can be controlled using the midi input data
/// and read to produce & modify waves
#[derive(Clone, Deserialize, Serialize)]
pub struct Note {
    /// midi note
    /// between 0 and 127
    pub freq: u8,
    /// initial note velocity
    /// between 0 and 127
    pub vel: u8,
    /// botch pitch control values
    /// will be bit shiftet to represent one larger number
    pub pitch: (u8, u8),
    /// pressure send by midi device
    pub pressure: u8,
}
impl Note {
    pub fn from_midi(freq: u8, vel: u8) -> Note {
        Note {
            freq,
            vel,
            pitch: (0, 0),
            pressure: 0,
        }
    }
}
