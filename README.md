# PolarPlayer Studio

![PolarPlayer Studio Banner](./assets/banner.png)

## About

PolarPlayer Studio is a MIDI synth for the ROLI Seaboard
and other MIDI instruments that transmit pressure data using channel aftertouch.

As of version 2.0 the sdl core has been replaced with [miniquad](https://github.com/not-fl3/miniquad) and [egui](https://github.com/emilk/egui) [miniquad bindings](https://github.com/not-fl3/egui-miniquad).
In theory miniquad should allow PolarPlayer Studio to run an nearly every device, but only Linux has been confirmed to be working.

As of version 3.0 the patchbay has been partially rewritten,
and it now allows for different port types.
Following this evolution,
the midi input has been moved from the global application
into its own patchbay node.
Additionally version 3.0 adds the long awaited preset menu.
This allows you to store your favorite configurations for later use.


## Getting started

Because there are no prebuild binaries available you have to build PolarPlayer Studio yourself,
but don't let that scare you

-   it shouldn't take long.
    
    > Hey!
    > If you are on Alpine Linux,
    > PolarPlayer Studio can now be found in my APKBUILD repository:
    > [on codeberg](https://codeberg.org/comcloudway/apks)


### Requirements

As mentioned above only Linux has been confirmed to be supported.

If you are running Linux,
then hey you are in luck,
you just have to follow the instructions.

If you are not running Linux,
well you are kind of on your own.

**NOTE: I was able to compile the android version, but touch input does not appear to be working, nor is there midi support for android**

Either way here is a list of tools needed to build PolarPlayer Studio:

-   git [recommended]
-   rust and cargo, using [rustup](https://rustup.rs/) is recommended
-   if you want to try to build the android version, you might want to try [crossbundle](https://github.com/dodorare/crossbow)


### Grabbing the source

Lets get going by downloading the source.
The only official repository is hosted over on [codeberg](https://codeberg.org/comcloudway/polarplayer-studio).

Start by navigating into your Projects folder,
or your Downloads folder,
depending on where you want to store the source code.
(I personally like storing software others made in `$HOME/.local/opt`)

After navigating into the folder you have to download the source code.
You can either download the source code as a .zip
and extract it in a seperate folder,
or you use `git` to clone the repository,
which is recommended as it allows you to update the source code easier.

    git clone https://codeberg.org/comcloudway/polarplayer-studio


### Performing a check and doing a test run

Although you could build the binary immediately,
you should make sure that the source code does not contain any errors,
and that PolarPlayer Studio does work on your system.

You can achive this by navigating into this projects directory,
and running `cargo check`,
which will check if the projects contains any errors.
Additionally cagro check also pulls all the dependencies used by this project.

After making sure that the version you downloaded does not contain errors,
you are ready to run it.
Running PolarPlayer Studio is as easy as
typing `cargo run --release` into your command line
and waiting for the window to open.

**NOTE: If you plan on using midi you have to append `--features midi` to the previous commands, to enable the midi feature**

If PolarPlayer Studio launches successfully you are ready to build the binary.


### Finally building the binary

Here comes the fifth and last command: To build PolarPlayer Studio as a binary/executable replace the run in the previous command with build.

    # without midi features (for unsupported platforms, e.g. android)
    cargo build --release
    # with midi support
    cargo build --release --features midi



## Troubleshooting


### ALSA lib seq<sub>hw.c</sub>:466:(snd<sub>seq</sub><sub>hw</sub><sub>open</sub>) open /dev/snd/seq failed: Permission denied

I get the same error message on my artix install
and even though my first though was to use doas,
that would have required me to reinstall everything.

So I did a quick startpage search,
which revealed that one has to modproble `snd_seq`.

After running on of the following commands you have to restart PolarPlayer Studio.

    # using sudo
    sudo modprobe snd_seq
    
    # using doas
    doas modprobe snd_seq


### Sound is lagging behind

If your speakers keep producing clicking sounds,
this indicates that PolarPlayer Studio isn&rsquo;t running as fast
as it is supposed to be running.
This might happen if you build the debug version instead of the release version.

To solve this rebuild the binary using the `--release` flag

    cagro build --release --features midi


## Screenshots

![img](./screenshots/basic.png "Basic Player View")

![img](./screenshots/setup.png "Advanced configuration")

![img](./screenshots/popups.png "About popup")
